package main

import (
	"fmt"
	"os"
	"strconv"
	"bitbucket.org/vinaysshenoy/facebookclient"
)

func main() {
	
	var i,err = strconv.ParseFloat(os.Args[1], 64)
	
	if err == nil {
		fmt.Printf("Hello, world. Sqrt(%v) = %v\n", i, facebookclient.Sqrt(i))
	} else {
		fmt.Printf(err.Error())
	}
}
